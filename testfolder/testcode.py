import time
from functools import wraps


def my_exec_func(func):
    @wraps(func)
    def my_exec_inner(*args, **kwargs):
        print("{}함수가 실행됩니다.".format(func.__name__))
        return func
    return my_exec_inner


def my_timer_func(func):
    @wraps(func)
    def my_timer_inner(*args, **kwargs):
        print("{} 함수수행 시간을 계산합니다.".format(func.__name__))
        start=time.time()
        func(*args, **kwargs)
        end=time.time()-start
        print("{} 함수수행 시간은 {} 입니다.".format(func.__name__,end))

    return my_timer_inner


@my_exec_func
@my_timer_func
def my_adder(x,y):
    print("두 수를 더한 결과는 {}".format(x+y))


my_adder(10,20)


@my_timer_func
@my_exec_func
def my_adder(x,y):
    print("두 수를 더한 결과는 {}".format(x+y))


my_adder(10,20)

my_adder.__doc__
my_adder.__name__