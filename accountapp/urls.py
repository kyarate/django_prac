from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path

#app name 명시
#127.0.0.1:8000/account/hello_world
#이걸 항상 쓰기 번거로우니,
#accountapp:hello_world 라고 쓰면 위가 저절로 쓰여짐
from accountapp.views import AccountCreateView, AccountDetailView, AccountUpdateView, AccountDeleteView, hello_world

app_name = 'accountapp'

urlpatterns = [
    path('hello_world/', hello_world, name='hello_world'),

    path('login/', LoginView.as_view(template_name ='accountapp/login.html'), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),

    path('create/', AccountCreateView.as_view(), name='create'),
    path('detail/<int:pk>', AccountDetailView.as_view(), name='detail'),   # int형 pk이름의 변수를 받겠따
    path('update/<int:pk>', AccountUpdateView.as_view(), name='update'),
    path('delete/<int:pk>', AccountDeleteView.as_view(), name='delete'),
]