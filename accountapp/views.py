from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, DetailView, UpdateView, DeleteView

from accountapp.decorators import account_ownership_required
from accountapp.models import HelloWorld


# Create your views here.

##이 뷰를 돌아가게 끔 하기 위해  연결하는 과정(routing: 특정주소를 갔을때) 고로 root.urls.py 에서 연결한다
from accountapp.templates.accountapp.forms import AccountUpdateForm

has_ownership = [account_ownership_required, login_required]


'''
@login_required  
    if request.user.is_authenticated:     
    
    else :
        return HttpResponseRedirect(reverse('accountapp:login'))
        
    을 decorating한다.


'''
@login_required
def hello_world(request):

    if request.method =="POST":

        temp = request.POST.get('hello_world_input')

        new_hello_world = HelloWorld()
        new_hello_world.text = temp
        new_hello_world.save() #DB 저장

        return HttpResponseRedirect(reverse('accountapp:hello_world'))

    else :
        hello_world_list = HelloWorld.objects.all()  # 이러면 모든 객체를 다 긁어온다.
        return render(request, 'accountapp/hello_world.html', context ={'hello_world_list':hello_world_list})



class AccountCreateView(CreateView) :
    model = User
    form_class = UserCreationForm  #장고가 유저 계정정보 폼을 제공한다.
    success_url = reverse_lazy('accountapp:hello_world')  #어디로 연결할건지 설정한다
    template_name = 'accountapp/create.html'


    '''
    redirect함수 인 reverse, 와 reverse_lazy는 함수와 클래스 차이이기때문
    '''

class AccountDetailView(DetailView) :
    model = User
    context_object_name= 'target_user'
    template_name = 'accountapp/detail.html'


@method_decorator(has_ownership, 'get')
@method_decorator(has_ownership, 'post')
class AccountUpdateView(UpdateView) :
    model = User
    context_object_name = 'target_user'
    form_class = AccountUpdateForm  #장고가 유저 계정정보 폼을 제공한다.
    success_url = reverse_lazy('accountapp:hello_world')  #어디로 연결할건지 설정한다
    template_name = 'accountapp/update.html'


@method_decorator(has_ownership, 'get')
@method_decorator(has_ownership, 'post')
class AccountDeleteView(DeleteView):
    model = User
    context_object_name = 'target_user'
    success_url = reverse_lazy('accountapp:login')
    template_name = 'accountapp/delete.html'


    '''
    def get(self, *args, **kwargs):
        if self.request.user.is_authenticated and self.get_object() == self.request.user:
            return super().get(*args, **kwargs)
        else :
            return HttpResponseForbidden()

    def get(self, *args, **kwargs):
        if self.request.user.is_authenticated and self.get_object() == self.request.user:
            return super().post(*args, **kwargs)
        else:
            return HttpResponseForbidden()
    
    '''